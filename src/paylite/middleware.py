import logging

from aiohttp import web
from marshmallow import ValidationError

from paylite.exceptions import LogicalException

logger = logging.getLogger(__name__)


@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        if response.status != 404:
            return response
        message = response.message
        status = response.status
    except LogicalException as ex:
        logger.exception(ex)
        message = f'{ex.__class__.__qualname__} {ex}'
        status = 500
    except ValidationError as ex:
        logger.exception(ex)
        message = ex.messages
        status = 400
    except ValueError as ex:
        logger.exception(ex)
        message = str(ex)
        status = 400
    except web.HTTPException as ex:
        logger.exception(ex)
        if ex.status != 404:
            raise
        message = ex.reason
        status = 500

    return web.json_response({'status': 'fail', 'error': message}, status=status)
