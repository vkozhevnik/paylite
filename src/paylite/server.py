import asyncio
import logging.config

import aiopg
from aiohttp import web

from paylite.middleware import error_middleware
from paylite.settings import PSQL_DSN, LOGGING
from paylite.tasks import load_currency_rates
from paylite.views import balance_increase, currency_exchange, money_transfer, register_user, GetTransactionHistory


async def create_aiopg(app):
    app['pg_pool'] = await aiopg.create_pool(PSQL_DSN)


async def dispose_aiopg(app):
    app['pg_pool'].close()
    await app['pg_pool'].wait_closed()


async def init_app():
    app = web.Application(middlewares=[error_middleware])
    app.router.add_get('/api/currency/rate', currency_exchange)
    app.router.add_post('/api/user/register', register_user)
    app.router.add_post('/api/user/money_transfer', money_transfer)
    app.router.add_post('/api/user/balance_increase', balance_increase)
    app.router.add_get('/api/user/wallet/history', GetTransactionHistory().do_route)

    app.on_startup.append(create_aiopg)
    app.on_cleanup.append(dispose_aiopg)

    return app


async def init():
    app = await init_app()

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 8080)
    await site.start()


if __name__ == '__main__':
    logging.config.dictConfig(LOGGING)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(load_currency_rates())
        loop.run_until_complete(init())
        loop.run_forever()
    except KeyboardInterrupt:
        loop.stop()
        loop.close()
