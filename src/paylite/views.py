import functools

from aiohttp import web
from aiohttp.web_exceptions import HTTPNotAcceptable

from paylite.exceptions import CursorAlreadySet
from paylite.managers import UserManager, WalletManager
from paylite.shemes import (
    UserRegistrationSchema, MoneyTransferSchema, BalanceIncreaseSchema, TransactionHistorySchema,
    TransactionHistoryQuerySchema, CurrencyRatesQuerySchema)
from paylite.utils import json_response


def pg_cursor_middleware(func):
    @functools.wraps(func)
    async def wraps(request, *args, **kwargs):
        if 'cursor' in kwargs.keys():
            raise CursorAlreadySet()
        async with request.app['pg_pool'].acquire() as conn:
            async with conn.cursor() as cursor:
                return await func(request, *args, cursor=cursor, **kwargs)
    return wraps


@pg_cursor_middleware
async def register_user(request, cursor):
    data = await request.json()
    result = UserRegistrationSchema(strict=True).load(data)
    user_id = await UserManager(cursor=cursor).register_user(data=result.data)
    return json_response({'status': 'ok', 'data': {'user_id': user_id}})


@pg_cursor_middleware
async def money_transfer(request, cursor):
    json_data = await request.json()
    result = MoneyTransferSchema(strict=True).load(json_data)
    data = result.data
    await WalletManager(cursor=cursor).transfer_money(
        from_user=data['user_from_id'],
        to_user=data['user_to_id'],
        amount=data['amount'], currency=data['currency'])
    return json_response({'status': 'ok', 'data': {}})


@pg_cursor_middleware
async def balance_increase(request, cursor):
    json_data = await request.json()
    result = BalanceIncreaseSchema(strict=True).load(json_data)

    data = result.data
    await WalletManager(cursor=cursor).increase_balance(
        user_id=data['user_id'],
        amount=data['amount'], currency=data['currency'])
    return json_response({'status': 'ok', 'data': {}})


class GetTransactionHistory(object):

    def __init__(self):
        self._accepts = {
            'application/json': handle_json,
            'application/csv': handle_csv,
            'application/xml': handle_xml,
        }

    async def do_route(self, request):
        for accept in request.headers.getall('ACCEPT', []):
            acceptor = self._accepts.get(accept)
            if acceptor is not None:
                return (await acceptor(request))
        raise HTTPNotAcceptable()


async def _prepare_data(request, cursor):
        query = TransactionHistoryQuerySchema(strict=True).load(request.query)
        result = await WalletManager(cursor=cursor).get_transaction_history(
            user_id=query.data['user_id'],
            date_from=query.data.get('date_from'),
            date_to=query.data.get('date_to'),
        )
        res = TransactionHistorySchema(many=True).dump(result)
        return res.data


@pg_cursor_middleware
async def handle_json(request, cursor):
    result = await _prepare_data(request=request, cursor=cursor)
    return json_response({'status': 'ok', 'data': result})


@pg_cursor_middleware
async def handle_xml(request, cursor):
    result = await _prepare_data(request=request, cursor=cursor)
    header = "<Transactions>{}</Transactions>"
    response = []
    for row in result:
        response.append(
            f'<Transaction id="{row["transaction_id"]}" type="{row["transaction_type"]}" '
            f'created_at="{row["created_at"]}" amount="{row["amount"]}" '
            f'currency="{row["currency"]}">{row["reason"]}</Transaction>')
    text = header.format('\n'.join(response))
    return web.Response(text=text, content_type='application/xml')


@pg_cursor_middleware
async def handle_csv(request, cursor):
    result = await _prepare_data(request=request, cursor=cursor)
    text = ','.join(["transaction_id", "transaction_type", "created_at", "amount", "currency", "reason", '\n'])
    for row in result:
        text += ','.join(
            map(str, [
                row["transaction_id"], row["transaction_type"], row["created_at"],
                row["amount"], row["currency"], row["reason"], '\n']))
    return web.Response(text=text, content_type='application/csv')


@pg_cursor_middleware
async def currency_exchange(request, cursor):
    query = CurrencyRatesQuerySchema(strict=True).load(request.query)
    result = await WalletManager(cursor=cursor).get_currency_rates(date=query.data['date'])
    return json_response({'status': 'ok', 'data': result})
