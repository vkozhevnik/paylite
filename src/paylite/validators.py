from marshmallow import ValidationError
from paylite.settings import ALLOWED_CURRENCY


def validate_currency(value):
    if value not in ALLOWED_CURRENCY:
        raise ValidationError('Currency {} not valid')
    return value
