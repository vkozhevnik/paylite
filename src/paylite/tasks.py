import json

import aiohttp
import aiopg

from paylite.managers import CurrencyExchangeManager
from paylite.settings import ALLOWED_CURRENCY, PSQL_DSN


async def load_currency_rates():
    uri = f"https://ratesapi.io/api/latest?base=USD&symbols={','.join(ALLOWED_CURRENCY)}"
    async with aiohttp.ClientSession() as session:
        async with session.get(uri, json={'test': 'object'}) as resp:
            result = json.loads(await resp.text())
    async with await aiopg.create_pool(PSQL_DSN) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await CurrencyExchangeManager(cursor=cursor).save_new_rates(
                    date=result['date'], rates=result['rates'])
