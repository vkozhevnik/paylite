ALLOWED_CURRENCY = ['USD', 'EUR', 'CAD', 'CRY']
PSQL_DSN = 'dbname=paylite user=postgres password=postgres host=127.0.0.1'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': True
        },
        'paylite': {
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': False
        },
    }
}
