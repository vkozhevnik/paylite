import decimal
import json
from functools import partial

from aiohttp import web

dsn = 'dbname=paylite user=postgres password=postgres host=127.0.0.1'


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return json.JSONEncoder.default(self, o)


def json_response(data, *, text=None, body=None, status=200,
                  reason=None, headers=None, content_type='application/json',):
    return web.json_response(
        data=data, text=text, body=body, status=status,
        reason=reason, headers=headers, content_type=content_type,
        dumps=partial(json.dumps, cls=DecimalEncoder))
