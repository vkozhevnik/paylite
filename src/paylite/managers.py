import datetime
import logging
from collections import namedtuple
from decimal import Decimal

from aiopg import Transaction, IsolationLevel, Cursor

from paylite.exceptions import CurrencyNotDefinedOnDate, UserNotFound, NotEnoughMoney, DontHaveCurrencyRates

logger = logging.getLogger(__name__)


class BaseManager(object):
    def __init__(self, cursor: Cursor):
        self._cursor = cursor


class UserManager(BaseManager):

    async def register_user(self, data):
        async with Transaction(self._cursor, isolation_level=IsolationLevel.read_committed):

            await self._cursor.execute(
                "insert into core_users (username, country, city) values "
                "('{username}', '{country}', '{city}') RETURNING id;".format(
                    **data))
            user_id, = await self._cursor.fetchone()

            await self._cursor.execute(
                f"INSERT INTO payment_wallets (user_id, currency, balance) "
                f"VALUES ({user_id}, '{data['currency']}', 0);"
            )
            return user_id


class CurrencyExchangeManager(BaseManager):

    async def insert_currency(self, *, date, currency, value):
        await self._cursor.execute(
            f"select 1 from currencies_rate where date='{date}' and currency='{currency}';")
        state = await self._cursor.fetchone()
        if state:
            logger.info(f'Already exists {date} {currency}')
            return

        await self._cursor.execute(
            f"insert into currencies_rate (date, currency, rate) values "
            f"('{date}', '{currency}', {value});")

    async def save_new_rates(self, date, rates):
        date = datetime.date.today()
        await self.insert_currency(date=date, currency='USD', value=1)
        for currency, value in rates.items():
            await self.insert_currency(date=date, currency=currency, value=value)

        await self._cursor.execute(f"REFRESH MATERIALIZED VIEW currency_rate_view;")


class WalletHistory(namedtuple('WalletHistory', 'transaction_id,transaction_type,created_at,amount,currency,reason')):
    pass


class WalletManager(BaseManager):

    async def get_transaction_history(self, *, user_id, date_from=None, date_to=None):
        extra = ""
        if date_from is not None and date_to is not None:
            extra = f"and pt.created_at between '{date_from}' and '{date_to}'"
        elif date_from is not None:
            extra = f"and pt.created_at > '{date_from}'"
        elif date_to is not None:
            extra = f"and pt.created_at < '{date_to}'"

        await self._cursor.execute(
            f"select pt.id as transaction_id, pt.type as transaction_type, pt.created_at as created_at, "
            f"pt.amount as amount, pw.currency as currency, ti.reason as reason "
            f"from payment_transactions as pt "
            f"left join payment_wallets pw on pt.user_id = pw.user_id "
            f"left join transaction_invoices ti on pt.invoice_id = ti.id "
            f"where pt.user_id = {user_id} {extra};"
        )
        data = []
        async for row in self._cursor:
            data.append(WalletHistory(*row))
        return data

    async def get_currency_rates(self, *, date=datetime.date.today()):
        await self._cursor.execute(
            f"SELECT currency_rate_view.data from currency_rate_view where date = '{date}';"
        )
        currency_rates, = await self._cursor.fetchone()
        if not currency_rates:
            raise DontHaveCurrencyRates()
        return currency_rates

    async def get_currency_rate(self, *, currency):
        date = datetime.date.today()
        currency_rates = await self.get_currency_rates(date=date)
        if currency not in currency_rates:
            raise CurrencyNotDefinedOnDate(f'Currency {currency} on date {date}')
        currency_rate = currency_rates[currency]
        return currency_rate

    async def get_users_wallet(self, user_ids):
        # GET USERS
        users = tuple(user_ids) if len(user_ids) > 1 else f'({(user_ids[0])})'
        await self._cursor.execute(
            f"SELECT payment_wallets.user_id, payment_wallets.balance, payment_wallets.currency "
            f"from payment_wallets where user_id in {users};")
        users = {}
        async for row in self._cursor:
            user_id, balance, user_currency = row
            users[user_id] = {
                'balance': balance,
                'currency': user_currency
            }
        for user_id in user_ids:
            if user_id not in users:
                raise UserNotFound(f'User {user_id}')
        return users

    async def increase_balance(self, *, user_id, amount, currency):
        async with Transaction(self._cursor, isolation_level=IsolationLevel.read_committed):
            users = await self.get_users_wallet(user_ids=[user_id])
            currency_rates = await self.get_currency_rates()
            currency_rate = currency_rates[currency]
            result_sum = Decimal(amount) / Decimal(currency_rate)  # in USD
            user_balance_sum = result_sum * Decimal(currency_rates[users[user_id]['currency']])

            await self._cursor.execute(
                f"insert into transaction_invoices (reason) values "
                f"('Increase balance {user_id}') RETURNING id;"
            )

            invoice_id, = await self._cursor.fetchone()
            await self._cursor.execute(
                f"INSERT INTO payment_transactions "
                f"(user_id, invoice_id, amount, currency, amount_usd, exchange_rate, type) VALUES " \
                f"({user_id}, {invoice_id}, {user_balance_sum}, '{currency}',"
                f" {result_sum}, '{currency_rate}'::NUMERIC , 'income') ;"
            )
            await self._cursor.execute(
                f"UPDATE payment_wallets "
                f"set balance = balance + '{user_balance_sum}'::NUMERIC where user_id = {user_id};"
            )

    async def transfer_money(self, *, from_user, to_user, amount, currency):
        async with Transaction(self._cursor, isolation_level=IsolationLevel.read_committed):
            users = await self.get_users_wallet(user_ids=[from_user, to_user])
            currency_rates = await self.get_currency_rates()
            currency_rate = currency_rates[currency]
            result_sum = Decimal(amount) / Decimal(currency_rate)  # in USD
            from_user_balance_sum = result_sum * Decimal(currency_rates[users[from_user]['currency']])
            to_user_balance_sum = result_sum * Decimal(currency_rates[users[to_user]['currency']])

            if users[from_user]['balance'] - from_user_balance_sum < 0:
                raise NotEnoughMoney()

            await self._cursor.execute(
                f"insert into transaction_invoices (reason) values "
                f"('Money transfer from {from_user} to {to_user}') RETURNING id;"
            )

            invoice_id, = await self._cursor.fetchone()
            await self._cursor.execute(
                f"INSERT INTO payment_transactions "
                f"(user_id, invoice_id, amount, currency, amount_usd, exchange_rate, type) VALUES " \
                f"({from_user}, {invoice_id}, {-from_user_balance_sum}, '{currency}', "
                f"{result_sum}, '{currency_rate}'::NUMERIC , 'outcome'), " \
                f"({to_user}, {invoice_id}, {to_user_balance_sum}, '{currency}', "
                f"{result_sum}, '{currency_rate}'::NUMERIC , 'income') ;"
            )
            await self._cursor.execute(
                f"UPDATE payment_wallets set balance = balance + '{to_user_balance_sum}'::NUMERIC "
                f"where user_id = {to_user};"
            )
            await self._cursor.execute(
                f"UPDATE payment_wallets set balance = balance - '{from_user_balance_sum}'::NUMERIC "
                f"where user_id = {from_user};"
            )
