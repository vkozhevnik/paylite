CREATE TABLE currencies_rate (
  date    DATE          NOT NUll,
  currency CHAR(3)       NOT NULL,
  rate    NUMERIC(9, 6) NOT NULL,
  PRIMARY KEY(date, currency)
);

CREATE MATERIALIZED VIEW currency_rate_view as
SELECT date, json_object_agg(currency, rate) as data from currencies_rate group by date;

CREATE UNIQUE INDEX date_uniq_idx on currency_rate_view(date);

DROP MATERIALIZED VIEW currency_rate_view;

CREATE TABLE core_users(
  id SERIAL PRIMARY KEY,
  username VARCHAR(64) NOT NULL,
  country VARCHAR(64) NOT NULL,
  city VARCHAR(64) NOT NULL
);

CREATE TABLE payment_wallets(
  user_id INT REFERENCES core_users(id) PRIMARY KEY,
  currency CHAR(3) NOT NULL,
  balance NUMERIC(10, 2)
);

CREATE TABLE transaction_invoices(
  id bigserial primary key,
  reason text
);

CREATE TYPE transaction_type as  ENUM('income', 'outcome');

CREATE TABLE payment_transactions(
  id BIGSERIAL PRIMARY KEY,
  user_id INT REFERENCES core_users(id) NOT NULL,
  invoice_id INT8 REFERENCES transaction_invoices(id),
  created_at TIMESTAMP WITH TIME ZONE default now(),
  amount NUMERIC(10, 2) NOT NULL,
  currency CHAR(3) NOT NULL,
  amount_usd NUMERIC(10, 2) NOT NULL,
  exchange_rate NUMERIC(9, 6) NOT NULL,
  type transaction_type NOT NULL
);

ALTER TABLE payment_transactions add constraint payment_transactions_amount_check check (
  (amount > 0 and type = 'income'::transaction_type)
  or (amount < 0 and type = 'outcome'::transaction_type)
);
