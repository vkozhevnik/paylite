from marshmallow import fields, Schema

from paylite.validators import validate_currency


class UserRegistrationSchema(Schema):
    username = fields.String(required=True)
    country = fields.String(required=True)
    city = fields.String(required=True)
    currency = fields.String(validate=validate_currency, required=True)


class MoneyTransferSchema(Schema):
    user_from_id = fields.Integer(required=True)
    user_to_id = fields.Integer(required=True)
    amount = fields.Decimal(places=2, required=True)
    currency = fields.String(validate=validate_currency, required=True)


class BalanceIncreaseSchema(Schema):
    user_id = fields.Integer(required=True)
    amount = fields.Decimal(places=2, required=True)
    currency = fields.String(validate=validate_currency, required=True)


class TransactionHistorySchema(Schema):
    transaction_id = fields.Integer()
    transaction_type = fields.String()
    created_at = fields.DateTime()
    amount = fields.Decimal(places=2)
    currency = fields.String()
    reason = fields.String()


class TransactionHistoryQuerySchema(Schema):
    user_id = fields.Integer(required=True)
    date_from = fields.Date()
    date_to = fields.Date()


class CurrencyRatesQuerySchema(Schema):
    date = fields.Date(required=True)
