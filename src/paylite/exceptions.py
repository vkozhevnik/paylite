class LogicalException(Exception):
    pass


class NotEnoughMoney(LogicalException):
    pass


class DontHaveCurrencyRates(LogicalException):
    pass


class UserNotFound(LogicalException):
    pass


class CurrencyNotDefinedOnDate(LogicalException):
    pass


class CursorAlreadySet(Exception):
    pass
