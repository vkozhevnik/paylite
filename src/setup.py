from distutils.core import setup

import setuptools

setup(
    name='paylite',
    version="0.0.1",
    packages=setuptools.find_packages(),
)